﻿using System;

namespace GrowingPlant
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(growingPlant(100, 10, 910));
        }

        static int growingPlant(int upSpeed, int downSpeed, int desiredHeight) {
            if(upSpeed > desiredHeight){
                return 1;
            }
            
            int height = 0;
            int days = 0;
            
            while(height <= desiredHeight){
                days++;
                height += upSpeed;
                if(height >= desiredHeight) return days;
                height -= downSpeed;
                if(height >= desiredHeight) return days;
            }
            
            return days;
        }

    }
}
